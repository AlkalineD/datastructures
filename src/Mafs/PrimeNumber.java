package Mafs;

import java.util.ArrayList;

public class PrimeNumber {
    static boolean isPrime(int number) {
        boolean result = true;
        int i = 2;
        while (i < number / 2) {
            if (number % i == 0) {
                result = false;
            }
            ++i;
        }
        if (number <= 1) {
            result = false;
        }
        return result;
    }

    static ArrayList primeInRange(int lower, int upper) {
        ArrayList<Integer> primes = new ArrayList<>();
        int i = lower;
        while (i <= upper) {
            if (isPrime(i)) {
                primes.add(i);
            }
            ++i;
        }

        return primes;
    }

    public static void main(String[] args) {
        System.out.println(isPrime(5));
    }
}
