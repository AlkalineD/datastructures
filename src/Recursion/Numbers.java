package Recursion;


class Recursive {

    int recursiveSum(int number) {
        if (number == 0) {
            return 0;
        }
        int sumOfPrev = recursiveSum(number - 1);
        return number + sumOfPrev;    // current number + sum of all previous number (k + (k-1 + k-2 +------ + 2 + 1 + 0))
    }

    int mPowerN(int m, int n) {        // m^n
        if (n == 1) {   // m^1 = m
            return m;
        }
        int currentProd = mPowerN(m, n - 1);
        return m * currentProd;
    }

    long factorial(long n) {
        if (n < 0) {
            return Long.MIN_VALUE;
        }
        if (n == 0 || n == 1) {
            return 1;
        }
        long fact = factorial(n - 1);
        return n * fact;
    }

    int nThFibonacci(int n) { // 0 1 1 2 3 5 8 13 21 34 -----------
        if (n == 0) { // 0th term is zero
            return 0;
        } else if (n == 1 || n == 2) { // first and second terms are both 1
            return 1;
        }
        return nThFibonacci(n - 1) + nThFibonacci(n - 2);
    }

    boolean isSorted(int[] array, int n) {
        if (n == 0 || n == 1) {
            return true;
        }
        if (array[n - 1] < array[n - 2]) {
            return false;
        }

        return isSorted(array, n - 1) && isSorted(array, n - 1);
    }

    void decreaseTillN(int n) {
        if (n == 0) {
            return;
        }
        System.out.print(n + " ");
        decreaseTillN(n - 1);
    }

    void increaseTillN(int n) {
        if (n == 0) {
            return;
        }
        increaseTillN(n - 1);
        System.out.print(n + " ");
    }

    //  first and last occurrence of an element in array

    int firstOccurrence(int[] array, int arrayPointer, int key) {
        if (array[arrayPointer] == key) {
            return arrayPointer;
        } else if (arrayPointer > array.length) {
            return -1;
        }
        return firstOccurrence(array, arrayPointer + 1, key);

    }

    int lastOccurrence(int[] array, int arrayPointer, int key) {
        if (arrayPointer < 0) {
            return -1;
        }
        if (array[arrayPointer] == key) {
            return arrayPointer;
        }
        return lastOccurrence(array, arrayPointer - 1, key);
    }

    void printTheArray(int[] array, int arrayPointer) {
        if (arrayPointer == array.length) {
            return;
        }
        System.out.print(array[arrayPointer] + " ");
        printTheArray(array, arrayPointer + 1);
    }

    void reverseTheNumber(int number) {
        if (number / 10 <= 0) {
            System.out.println(number);
            return;
        }
        System.out.print(number % 10);
        reverseTheNumber(number / 10);
    }

    void factors(int number, int i) {
        if (i > number) {
            return;
        } else if (number % i == 0) {
            System.out.print(i + " ");
        }
        factors(number, i + 1);
    }


    // check
    boolean isPrime(int number, int i) {
        if (number <= 1) {
            return false;
        } else if (number % i != 0 && i < number) {
            return true;
        }
        return isPrime(number, i + 1);


    }

}

public class Numbers {
    public static void main(String[] args) {
        Recursive recursive = new Recursive();
        //  Recursive Sum
        System.out.println(recursive.recursiveSum(4));
        // Power using recursion
        System.out.println(recursive.mPowerN(4, 3));
        // Factorial
        System.out.println(recursive.factorial(14));
        // Fibonacci
        System.out.println(recursive.nThFibonacci(9));
        // Array is Sorted
        System.out.println(recursive.isSorted(new int[]{5, 7, 8, 9, 20, 2}, 6));

        recursive.decreaseTillN(10);
        System.out.println();
        recursive.increaseTillN(10);
        System.out.println();

        System.out.println(recursive.firstOccurrence(new int[]{1, 4, 5, 7, 9, 10, 2, 7}, 0, 7));
        System.out.println(recursive.lastOccurrence(new int[]{1, 4, 5, 7, 9, 10, 2, 7}, 7, 10));

        recursive.printTheArray(new int[]{1, 4, 5, 7, 9, 10, 2, 7}, 0);
        System.out.println();

        recursive.factors(16, 1);
        System.out.println();

        System.out.println(recursive.isPrime(15, 2));

        

    }
}
