package Recursion;

class UtilityClass {

    void reverse(String str) {
        if (str.length() <= 1) { // If String is empty, then exit.
            System.out.println(str);
            return;
        }
        System.out.print(str.charAt(str.length() - 1)); //  print the last character
        reverse(str.substring(0, str.length() - 1)); // recursion for left part of string (other then already printed)
    }


    int strLength(String str) {
        if (str.isEmpty()) {
            return 0;
        }
        int len = strLength(str.substring(1));
        return 1 + len;
    }

    /*
        Replace "pi" in a string with 3.14
     */

    void replacePiWithValue(String str) {
        if (str.isEmpty()) {
            return;
        }
        if (str.charAt(0) == 'p' && str.charAt(1) == 'i') { //  If pi was found, skip two places(p and i) and check for next substring
            try {
                System.out.print(3.14);
                replacePiWithValue(str.substring(2)); //    skipping 2 places
            } catch (StringIndexOutOfBoundsException indexOutOfBoundsException) {
                return;
            }
        } else {
            System.out.print(str.charAt(0)); // pi was not found, skip one place and check for next substring
            replacePiWithValue(str.substring(1)); //    skipping 1 place
        }
    }

    /*
        Tower of hanoi

        1. Move only one block at a timer
        2. At all time, no bigger block must be on top of smaller block
        3. Steps Required (2^n) - 1
     */
    void towerOfHanoi(int blockCount, char source, char destination, char extra) {
        if (blockCount == 0) {
            return;
        }
        towerOfHanoi(blockCount - 1, source, extra, destination);
        System.out.println(source + "------->" + destination);
        towerOfHanoi(blockCount - 1, extra, destination, source);
    }

    String removeDuplicates(String str) {   //  Hheellllo
        if (str.length() == 1) {
            return str;
        }
        char firstPosChar = str.charAt(0);
        String rightString = removeDuplicates(str.substring(1));

        if (firstPosChar == rightString.charAt(0)) {
            return rightString;
        }
        return rightString + firstPosChar;
    }

}

public class Strings {
    public static void main(String[] args) {
        UtilityClass utilityClass = new UtilityClass();
        utilityClass.reverse("Hello");

        System.out.println(utilityClass.strLength("CODM") == "CODM".length());

//        utilityClass.replacePiWithValue("BigPPpipipippip");
//        System.out.println();

//        utilityClass.towerOfHanoi(4, 'A', 'C', 'B');
        System.out.println(utilityClass.removeDuplicates("Hheellllo"));

    }
}
