package Queues;

class Queue {
    int[] queue;
    int rear, front;

    Queue() {
        queue = new int[10];
        rear = front = -1;
    }

    void enqueue(int data) {
        if (rear == -1 && front == -1) {
            rear = front = 0;
        } else {
            ++rear;
        }
        queue[rear] = data;
    }

    int peek() {
        if (rear == -1 && front == -1) {
            return 404;
        } else {
            return queue[front];
        }
    }

    int dequeue() {
        if (front == -1 && rear == -1) {
            return 404;
        } else {
            int value = queue[front];
            ++front;
            if (front == rear + 1) {
                front = rear = -1;
            }
            return value;
        }
    }

}

public class ArrayImplementation {
    public static void main(String[] args) {
        Queue queue = new Queue();
        queue.enqueue(2);
        queue.enqueue(3);
        queue.enqueue(4);
        System.out.println(queue.dequeue());
        System.out.println(queue.dequeue());
        System.out.println(queue.dequeue());
        System.out.println(queue.dequeue());
        queue.enqueue(34);
        System.out.println(queue.front + " " + queue.rear);
        queue.enqueue(56);
        System.out.println(queue.front + " " + queue.rear);
        System.out.println(queue.dequeue());
        System.out.println(queue.front + " " + queue.rear);
        System.out.println(queue.dequeue());
        System.out.println(queue.front + " " + queue.rear);
        System.out.println(queue.dequeue());
        System.out.println(queue.front + " " + queue.rear);
    }
}
