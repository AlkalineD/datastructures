package Queues;

class LinkedStructure {
    int data;
    LinkedStructure next;
}

class Queuee {
    int front, rear = -1;

    LinkedStructure Queue = new LinkedStructure();

    void enqueue(int data) {
        LinkedStructure traverser = new LinkedStructure();
        traverser = Queue;
        if (Queue == null) {
            front = rear = 0;
        } else {
            ++rear;
        }
        while (traverser.next != null) {
            traverser = traverser.next;
        }
        LinkedStructure node = new LinkedStructure();
        node.data = data;
        node.next = null;
        traverser.next = node;
    }

    void peek() {
        System.out.println("Peek:" + " " + Queue.next.data);
    }

    void dequeue() {
        if (front == rear) {
            Queue = null;
            front = rear = -1;
        } else {
            Queue = Queue.next;
            ++front;
        }
    }
}

// 12-1

public class LinkedQueue {
    public static void main(String[] args) {
        Queuee queuee = new Queuee();
        queuee.enqueue(1);
        queuee.enqueue(12);
        queuee.enqueue(13);
        queuee.enqueue(14);
        queuee.enqueue(15);
        System.out.println(queuee.front + " " + queuee.rear);
        queuee.peek();
        queuee.dequeue();
        queuee.peek();
        queuee.dequeue();
        queuee.peek();
        queuee.dequeue();
        queuee.peek();
        System.out.println(queuee.front + " " + queuee.rear);
    }
}
