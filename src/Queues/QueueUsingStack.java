package Queues;

import java.util.Stack;

class StackQueue {
    Stack<Integer> stackOne;
    Stack<Integer> stackTwo;

    StackQueue() {
        stackOne = new Stack<>();
        stackTwo = new Stack<>();
    }

    void enqueue(int data) {
        stackOne.push(data);
    }

    void dequeue() {
        if (stackOne.isEmpty()) {
            System.out.println("Empty Queue");
            return;
        } else {
            while (!stackOne.isEmpty()) {
                stackTwo.push(stackOne.pop());
            }
            System.out.println(stackTwo.pop());
        }
    }

    int peek() {
        while (!stackOne.isEmpty()) {
            stackTwo.push(stackOne.pop());
        }
        return stackTwo.peek();
    }
}

public class QueueUsingStack {
    public static void main(String[] args) {
        StackQueue queue = new StackQueue();
        queue.enqueue(10);
        queue.enqueue(20);
        queue.enqueue(30);
        queue.enqueue(40);
        queue.dequeue();
        System.out.println(queue.peek());
    }
}
