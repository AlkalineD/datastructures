package Stacks;

import java.util.Stack;

class Process {
    String expression;
    int top;
    String result;


    Process(String expression) {
        this.expression = expression;
        result = "";
    }


    int getPriority(char operand) {
        int priority = -1;
        if (operand == '^') {
            priority = 3;
        } else if (operand == '*' || operand == '/') {
            priority = 2;
        } else if (operand == '+' || operand == '-') {
            priority = 1;
        }
        return priority;
    }

    void infixToPostfix() {
        Stack<Character> stack = new Stack<>();
        for (int i = 0; i < expression.length(); ++i) {
            char currentChar = expression.charAt(i);
            if (Character.isLetterOrDigit(currentChar)) {
                result += currentChar;
            } else if (currentChar == '(') {
                stack.push(currentChar);
            } else if (currentChar == ')') {
                while (!stack.isEmpty() && stack.peek() != '(') {
                    result += stack.pop();
                }
                stack.pop();
            } else {
                while (!stack.isEmpty() && getPriority(currentChar) <= getPriority(stack.peek())) {
                    result += stack.pop();
                }
                stack.push(currentChar);
            }
        }

        while (!stack.isEmpty()) {
            if (stack.peek() == '(') {
                System.out.println("Brackets Imbalanced");
            } else {
                result += stack.pop();
            }
        }

        System.out.println("Result: " + result);
    }
}

public class InfixToPostfix {
    public static void main(String[] args) {
        Process process = new Process("a+b*(c^d-e)^(f+g*h)-i");
        process.infixToPostfix();
    }
}
