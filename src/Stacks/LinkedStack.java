package Stacks;

class LinkedStructure {
    int data;
    LinkedStructure next;
}

class Stackk {
    LinkedStructure stack = null;

    void push(int data) {
        LinkedStructure node = new LinkedStructure();
        node.data = data;
        node.next = stack;
        stack = node;
    }

    void pop() {
        stack = stack.next;
    }

    void peek() {
        System.out.println(stack.data);
    }
}


public class LinkedStack {
    public static void main(String[] args) {
        Stackk stackk = new Stackk();
        stackk.push(234);
        stackk.push(69);
        stackk.peek();
        stackk.pop();
        stackk.peek();
        stackk.pop();
        stackk.push(567);
        stackk.peek();

    }
}
