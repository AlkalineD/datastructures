package Stacks;

import java.util.Stack;

class PrefixConvertor {
    String expression;

    PrefixConvertor(String expression) {
        this.expression = expression;
    }

    int getPriority(int operator) {
        int priority = -1;
        if (operator == '^') {
            priority = 3;
        } else if (operator == '/' || operator == '*') {
            priority = 2;
        } else if (operator == '+' || operator == '-') {
            priority = 1;
        }

        return priority;
    }

    void convertor() {
        StringBuilder expressionNew = new StringBuilder(expression);
        expression = expressionNew.reverse().toString();
        Stack<Character> stack = new Stack<>();
        String result = "";
        for (int i = 0; i < expression.length(); ++i) {
            char currentChar = expression.charAt(i);
            if (Character.isLetterOrDigit(currentChar)) {
                result += currentChar;
            } else if (currentChar == ')') {
                stack.push(currentChar);
            } else if (currentChar == '(') {
                while (!stack.isEmpty() && stack.peek() != ')') {
                    result += stack.pop();
                }
                stack.pop();
            } else {
                while (!stack.isEmpty() && getPriority(currentChar) <= getPriority(stack.peek())) {
                    result += stack.pop();
                }
                stack.push(currentChar);
            }

            System.out.println(stack);

        }
        while (!stack.isEmpty()) {
            if (stack.peek() == ')') {
                System.out.println("Invalid Expression");
            } else {
                result += stack.pop();
            }
        }

        System.out.println("Result: " + new StringBuilder(result).reverse());


    }

}

public class InfixToPrefix {
    public static void main(String[] args) {
        PrefixConvertor prefixConvertor = new PrefixConvertor("(A-B/C)*(A/K-L)");
        prefixConvertor.convertor();

    }
}
