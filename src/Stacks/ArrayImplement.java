package Stacks;

class Stack{
    int[] array = new int[10];
    int top;

    Stack(){
        top = -1;
    }

    boolean isEmpty(){
        if(top == -1){
            return false;
        }else{
            return true;
        }
    }

    boolean isFull(){
        if(top == array.length - 1){
            return true;
        }else{
            return false;
        }
    }

    void push(int data){
        if(top < array.length - 1){
            top++;
            array[top] = data;
        }else{
            System.out.println("Failed!!");
        }
    }

    String pop(){
        int poped = array[top];
        --top;
        return "Deleted: "+ poped;
    }

    String peek(){
        return "Peeked: "+ array[top];
    }
}

public class ArrayImplement {
    public static void main(String[] args) {
        Stack stack = new Stack();
        System.out.println(stack.isEmpty());
        stack.push(456);
        stack.push(455);
        stack.push(457);
        stack.push(454);
        stack.push(453);
        System.out.println(stack.pop());
        System.out.println(stack.isEmpty());
        System.out.println(stack.peek());
    }
}
