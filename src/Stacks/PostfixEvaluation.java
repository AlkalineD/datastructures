package Stacks;

import java.util.Stack;

class Evaluator {
    String postFix;

    Evaluator(String postFix) {
        this.postFix = postFix;
    }

    void evaluation() {
        Stack<Integer> stack = new Stack<>();
        for (int i = 0; i < postFix.length(); ++i) {
            char currentChar = postFix.charAt(i);
            if (Character.isDigit(currentChar)) {
                stack.push(Integer.parseInt(currentChar+ ""));
            } else {
                int element1 = stack.pop();
                int element2 = stack.pop();
                int result;
                if (currentChar == '+') {
                    result = element1 + element2;
                    stack.push(result);
                } else if (currentChar == '-') {
                    result = element1 - element2;
                    stack.push(result);
                } else if (currentChar == '*') {
                    result = element1 * element2;
                    stack.push(result);
                } else if (currentChar == '/') {
                    result = element1 / element2;
                    stack.push(result);
                }
            }
            System.out.println(stack);
        }
        System.out.println(stack.pop());

    }
}

public class PostfixEvaluation {
    public static void main(String[] args) {
        Evaluator evaluator = new Evaluator("231*+9-");
        evaluator.evaluation();

    }
}
