package SinglyLinkedList;

import java.util.Stack;

class Structure {
    int data;
    Structure next;
}

class LinkedListMethods {
    Structure head = null;

    void insertFirst(int data) {
        Structure newNode = new Structure();
        newNode.data = data;
        newNode.next = head;
        head = newNode;
    }

    void deleteFirst() {
        head = head.next;
    }

    void insertLast(int data) {
        Structure traverser = head;
        while (traverser.next != null) {
            traverser = traverser.next;
        }
        Structure newNode = new Structure();
        newNode.data = data;
        traverser.next = newNode;
        newNode.next = null;
    }

    // 2 - 3 - 4 - 5 - 7/

    void deleteLast() {
        Structure traverser = head;
        while (traverser.next.next != null) {
            traverser = traverser.next;
        }
        traverser.next = null;

    }
    //3 - 4 - 6/ -8/

    void insertPos(int data, int position) {
        Structure traverser = head;
        try {
            int i = 0;
            while (i < position) {
                traverser = traverser.next;
                ++i;
            }
        } catch (Exception e) {
            System.out.println("Out of Bounds");
        }
        Structure newNode = new Structure();
        newNode.data = data;
        newNode.next = traverser.next;
        traverser.next = newNode;

    }

    // to-do
    void deletePos(int position) {
        Structure traverser = head;
        Structure traverserNext = head.next;

        try {
            int i = 0;
            while (i < position) {
                traverser = traverser.next;
                traverserNext = traverserNext.next;
                ++i;
            }
        } catch (Exception e) {
            System.out.println("Out of Bounds");
        }
        traverser.next = traverserNext.next;
    }

    void listPrinter() {
        Structure traverser = head;
        while (traverser != null) {
            System.out.print(traverser.data + " ");
            traverser = traverser.next;
        }
        System.out.println();
    }


    void search(int data) {
        int pos = 0;
        Structure traverser = head;
        while (traverser != null) {
            if (traverser.data != data) {
                traverser = traverser.next;
                ++pos;
            } else if (traverser.data == data) {
                System.out.println("Found at index " + "" + pos);
                return;
            }
        }
    }

    int getSize() {
        int sizeCounter = 1;
        Structure traverser = head;
        while (traverser.next != null) {
            traverser = traverser.next;
            ++sizeCounter;
        }
        return sizeCounter;

    }

    Structure getMiddleNode(Structure start, Structure last) {
        if (start == null) {
            return null;
        }

        Structure slow = start;
        Structure fast = start.next;

        while (fast != last) {
            fast = fast.next;
            if (fast != last) {
                slow = slow.next;
                fast = fast.next;
            }
        }
        return slow;
    }

    Structure getLastNode() {
        Structure traverser = new Structure();
        traverser = head;

        while (traverser.next != null) {
            traverser = traverser.next;
        }
        return traverser;
    }

    void binary(int data) {
        Structure start = head;
        Structure last = getLastNode();

        boolean found = false;

        while (start != last && !found) {
            Structure middle = getMiddleNode(start, last);
            if (middle.data == data) {
                System.out.println("Found");
                found = true;
            } else if (middle.data < data) {
                start = middle.next;
            } else {
                last = middle;
            }
        }
    }

    Stack<Integer> stack() {
        Stack<Integer> stack = new Stack<>();
        Structure traverser = head;
        while (traverser.next != null) {
            stack.push(traverser.data);
            traverser = traverser.next;
        }
        stack.push(traverser.data);

        return stack;
    }

    Structure nodeN(int n) {
        Structure traverser = head;
        int counter = 1;
        while (counter < n && traverser.next != null) {
            traverser = traverser.next;
            ++counter;
        }
        return traverser;
    }
}


class BasicOperations {
    public static void main(String[] args) {
        LinkedListMethods methods = new LinkedListMethods();
        methods.insertFirst(1);
        methods.insertLast(2);
        methods.insertLast(3);
        methods.insertLast(4);
        methods.insertLast(5);
        methods.insertLast(6);
        methods.listPrinter();
        methods.binary(5);
        Stack<Integer> elements = methods.stack();
        System.out.println(elements);

        LinkedListMethods reverseMethod = new LinkedListMethods();
        reverseMethod.insertFirst(elements.pop());
        reverseMethod.insertLast(elements.pop());
        reverseMethod.insertLast(elements.pop());
        reverseMethod.insertLast(elements.pop());
        reverseMethod.insertLast(elements.pop());
        reverseMethod.insertLast(elements.pop());
        reverseMethod.listPrinter();

        Structure nthNode = methods.nodeN(4);
        System.out.println(nthNode.data);
        System.out.println(nthNode.next.data);

    }
}