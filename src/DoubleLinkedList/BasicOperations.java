package DoubleLinkedList;

class LinkedStructure {
    int data;
    LinkedStructure leftNode;
    LinkedStructure rightNode;

    LinkedStructure() {
        data = 0;
    }

    LinkedStructure(int data) {
        this.data = data;
    }
}

class DoubleLinkMethods {
    LinkedStructure head = new LinkedStructure();

    void insertStart(int data) {
        LinkedStructure node = new LinkedStructure(data);
        node.leftNode = null;
        node.rightNode = head;
        head = node;
    }

    //  check
    void insertLast(int data) {
        LinkedStructure traverser = head;
        if (traverser == null) {
            LinkedStructure node = new LinkedStructure(data);
            node.leftNode = null;
            node.rightNode = null;
            head = node;
        } else {
            while (traverser.rightNode != null) {
                traverser = traverser.rightNode;
            }
            LinkedStructure node = new LinkedStructure(data);
            traverser.rightNode = node;
            node.leftNode = traverser;
            node.rightNode = null;
        }
    }

    void insertPosition(int data, int position) {
        if (head == null) {
            LinkedStructure node = new LinkedStructure(data);
            node.leftNode = null;
            node.rightNode = null;
            System.out.println("List was empty so inserted at start of list");
        } else {
            try {
                int positionCounter = 1;
                LinkedStructure traverser = head;
                while (positionCounter <= position && traverser.rightNode != null) {
                    traverser = traverser.rightNode;
                    ++positionCounter;
                }
                LinkedStructure node = new LinkedStructure(data);
                node.leftNode = traverser;
                node.rightNode = traverser.rightNode;
                traverser.rightNode = node;
                traverser.rightNode.leftNode = node;
                ++positionCounter;
            } catch (NullPointerException nullPointerException) {
                System.out.println("Probably position given for insertion is more than length of list");
            }
        }

    }

    void deleteStart() {
        head = head.rightNode;
        head.leftNode = null;
    }

    //  to-dp
    void deleteLast() {

    }

    void deletePos(int position) {

    }

    LinkedStructure nodeN(int n) {
        return null;
    }

    LinkedStructure getMiddleNode() {
        int size = getSize();
        int counter = 1;
        LinkedStructure traverser = head;
        while (counter < size && traverser.rightNode != null) {
            traverser = traverser.rightNode;
            ++counter;
        }
        return traverser;
    }

    int getSize() {
        LinkedStructure traverser = head;
        int sizeCounter = 1;
        while (traverser.rightNode != null) {
            traverser = traverser.rightNode;
            ++sizeCounter;
        }
        return sizeCounter;
    }

    void getList() {
        LinkedStructure traverser = head;
        if (traverser == null) {
            System.out.println("Empty List");
        } else {
            while (traverser.rightNode != null) {
                System.out.print(traverser.data + " ");
                traverser = traverser.rightNode;
            }
        }
        System.out.println();
    }

}

public class BasicOperations {
    public static void main(String[] args) {
        DoubleLinkMethods methods = new DoubleLinkMethods();
        methods.insertStart(1);
        methods.insertStart(2);
        methods.insertStart(4);
        methods.insertStart(7);
        methods.insertLast(55);
        methods.getList();
    }
}
