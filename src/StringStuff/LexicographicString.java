package StringStuff;

/*
    https://www.hackerrank.com/challenges/linkedin-practice-bigger-is-greater/problem?h_r=internal-search&isFullScreen=true

    Two strings are lexicographically equal if they are the same length and contain the same characters in the same positions

    compareTo(String) method can be used to check if two strings are lexicographically equal
        Usage: String.compareTo(AnotherString)
    It can return values as:
        0 : Strings are lexicographically equal
        +x: String > Another String
        -x: Another String > String

    For any a.compareTo(b), if a comes after b then the result is positive. If a comes before b, the result is negative.
    If the order of a and b is the same the result is 0.

 */

public class LexicographicString {
    public static void main(String[] args) {
        /*
            h = 104
            H = 72

            104 - 72 = 32
            72 - 104 = -32
         */

        System.out.println("Hello".compareTo("hello"));
        System.out.println("Hello".compareTo("Hello"));
        System.out.println("hello".compareTo("Hello"));

        String string1 = "Hello";
        String string2 = "Hemlo";
        String string3 = "melko";

        System.out.println(string1.compareTo(string2));
        System.out.println(string1.compareTo(string3));
        System.out.println(string2.compareTo(string3));

        System.out.println("ant".compareTo("anteater"));  // in different lengths, the difference of length is returned
        /*
            ant = 3
            anteater = 8
            8-3 = 5
         */
        System.out.println("anteater".compareTo("ant"));
        System.out.println();
        System.out.println();
        System.out.println(LexicographicString.compareStrings("Hello", "melko"));

    }

    static int compareStrings(String str1, String str2) {

        if (str1.length() != str2.length()) {   //In case of Different lengths, return difference of length
            return str1.length() - str2.length();
        } else {
            int i = 0;
            while (i < str1.length() && i < str2.length()) {
                if (str1.charAt(i) == str2.charAt(i)) {     // No difference found, move to next character
                    ++i;
                    continue;
                } else {
                    return (int) str1.charAt(i) - (int) str2.charAt(i);     // If difference was found, return the difference
                }
            }
        }

        return 0;
    }


}
