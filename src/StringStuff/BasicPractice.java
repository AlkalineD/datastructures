package StringStuff;

import java.util.Locale;

public class BasicPractice {
    public static void main(String[] args) {
        String x= "Hello";
        String y= "Hello";
        System.out.println(x == y);     // true  because x and y point to same location in string location pool

        String x1 = new String("Hello");
        String y1 = new String("Hello");
        System.out.println(x1 == y1);   // false because x and y are on different locations in string heap

//      String Methods
        System.out.println(x.toUpperCase());
        System.out.println(x.toLowerCase());
        System.out.println(x.length());
        System.out.println(x.concat("he"));
        System.out.println(x);

        int x2 = 32;
        double x21 = 32.1;
        String y2 = String.valueOf(x2);
        String y21 = String.valueOf(x21);
        System.out.println(x.intern());

        String z1 = x1.intern();
        System.out.println(x == z1);


    }
}
