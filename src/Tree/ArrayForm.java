package Tree;

class Implementation {
    int[] tree;
    int currentInsertPointer;
    int leftPos;
    int rightPos;

    Implementation(int treeSize, int rootData) {
        tree = new int[treeSize];
        for (int i = 0; i < tree.length; ++i) {
            tree[i] = -6969;
        }
        currentInsertPointer = 0;
        tree[0] = rootData;
    }


    void insert(int data) {
        leftPos = (2 * currentInsertPointer) + 1;
        rightPos = (2 * currentInsertPointer) + 2;
        if (tree[leftPos] == -6969) {
            tree[leftPos] = data;
        } else {
            tree[rightPos] = data;
        }
        if (tree[leftPos] != -6969 && tree[rightPos] != -6969) {
            ++currentInsertPointer;
        }
    }

    void deleteLast() {

    }

    void displayTree() {
        for (int x : tree) {
            System.out.print(x);
        }
    }

}

public class ArrayForm {
    public static void main(String[] args) {
        Implementation treeOne = new Implementation(5, 9);
        treeOne.insert(1);
        System.out.println(treeOne.currentInsertPointer);
        treeOne.insert(2);
        System.out.println(treeOne.currentInsertPointer);
        treeOne.insert(3);
        treeOne.insert(4);
        treeOne.displayTree();
    }
}