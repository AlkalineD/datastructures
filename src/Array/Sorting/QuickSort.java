package Array.Sorting;

class QuickSorter {

    void quickSort(int[] array, int left, int right) {
        if (left < right) {
            int pivotIndex = partition(array, left, right);
            quickSort(array, left, pivotIndex - 1);
            quickSort(array, pivotIndex + 1, right);
        }
    }

    int partition(int[] array, int left, int right) {
        int i = left - 1;
        for (int j = left; j < right; ++j) {
            if (array[j] < array[right]) {
                ++i;
                int temp = array[i];
                array[i] = array[j];
                array[j] = temp;
            }
        }
        int swap = array[i + 1];
        array[i + 1] = array[right];
        array[right] = swap;

        return i + 1;
    }

}

public class QuickSort {
    public static void main(String[] args) {
        int[] array = {4, 1, 6, 3, 5, 2};
        QuickSorter obj = new QuickSorter();
        obj.quickSort(array, 0, 5);
        for (int x : array) {
            System.out.print(x + " ");
        }
    }
}
