package Array.Sorting;

class Sort {
    void mergeSort(int[] array, int left, int right) {
        if (left < right) {  // if left = right it means only one element is left; so we divide the array until there there are n single elements
            int mid = (left + right) / 2;
            mergeSort(array, left, mid);
            mergeSort(array, mid + 1, right);

            merge(array, left, mid, right);
        }
    }

    void merge(int[] array, int left, int mid, int right) {

        // temporary array size
        int size1 = mid - left + 1;
        int size2 = right - mid;

        // temporary array
        int[] a = new int[size1];
        int[] b = new int[size2];

        // filling temporary array
        for (int i = 0; i < size1; ++i) {
            a[i] = array[left + i];
        }
        for (int i = 0; i < size2; ++i) {
            b[i] = array[mid + 1 + i];
        }

        // sorting begins
        // putting element into original array
        int i = 0;   // pointer for left array
        int j = 0;   // pointer for right array
        int k = left;   // pointer for original array

        while (i < size1 && j < size2) {
            if (a[i] < b[j]) {
                array[k] = a[i];
                ++i;
            } else {
                array[k] = b[j];
                ++j;
            }
            ++k;
        }

        // if one of the pointers doesnt reach the end of array, put it in original array
        while (i < size1) {
            array[k] = a[i];
            ++k;
            ++i;
        }
        while (j < size2) {
            array[k] = b[j];
            ++k;
            ++j;
        }

    }
}

public class MergeSort {
    public static void main(String[] args) {
        Sort sort = new Sort();
        int[] array = {38, 27, 43, 3, 9, 82, 10};
        sort.mergeSort(array, 0, array.length - 1);
        for (int x : array) {
            System.out.print(x + " ");
        }
    }
}
