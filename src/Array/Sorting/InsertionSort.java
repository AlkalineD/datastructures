package Array.Sorting;


/*
0 to 1-1  ===> sorted sub-array
i to array.length ===> possibly unsorted
 */

public class InsertionSort {
    public static void main(String[] args) {
        int[] array = {1, 6, 7, 2, 3, 0, 5, 4};
        for (int i = 1; i < array.length; ++i) {
            int j = i;
            while (j > 0 && array[j - 1] > array[j]) {
                int temp = array[j - 1];
                array[j - 1] = array[j];
                array[j] = temp;
                --j;
            }
        }
        for (int x : array) {
            System.out.print(x + " ");
        }
    }
}
