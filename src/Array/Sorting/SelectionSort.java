package Array.Sorting;

/*
smallest element is sorted for unsorted subarray

 */

public class SelectionSort {
    public static void main(String[] args) {
        int[] array = {5, 3, 2, 1, 4};
        for (int i = 0; i < array.length - 1; ++i) {
            int minIndex = i;
            for (int j = i + 1; j < array.length; ++j) {
                if (array[j] < array[minIndex]) {
                    minIndex = j;
                }
            }
            int swap = array[i];
            array[i] = array[minIndex];
            array[minIndex] = swap;
        }
        for (int x : array) {
            System.out.print(x + " ");
        }
    }
}
