package Array.Sorting;

/*
After each iteration next largest element bubbles to its required position
 */

public class BubbleSort {
    public static void main(String[] args) {
        int[] array = {4, 5, 7, 1, 3, 2, 6};

        int i;
        int j;
        for (i = 0; i < array.length - 1; ++i) {
            for (j = 0; j < array.length  - 1; ++j) {
                if (array[j] > array[j + 1]) {
                    int swap = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = swap;

                }
            }
        }
        System.out.print("Sorted: ");
        for (int x : array) {
            System.out.print(x + " ");
        }
    }
}
