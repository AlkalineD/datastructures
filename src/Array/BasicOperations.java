package Array;

import java.util.Arrays;

class Operations {

    int[] array;

    public Operations(int[] array) {
        this.array = array;
    }

    void traverse() {
        for (int element : array) {
            System.out.print(element + " ");
        }
        System.out.println();

    }

    void insert(int element, int index) {
        if (index >= array.length) {
            System.out.println("Cannot Insert");
        } else {
            for (int i = array.length - 1; i > 0; --i) {
                array[i] = array[i - 1];
            }
            array[index] = element;
        }
    }

    int delete(int index) {
        int deleted = 0;
        if (index >= array.length) {
            return -1;
        } else {
            deleted = array[index];
            for (int i = index; i < array.length - 1; ++i) {
                array[i] = array[i + 1];
            }
        }
        return deleted;
    }

    void deleteThis(int element) {
        int index = 0;
        while (true) {
            if (index > array.length - 1) {
                break;
            }
            if (array[index] != element) {
                ++index;
            } else if (array[index] == element) {
                break;
            }
        }
        for (int i = index; i < array.length - 1; ++i) {
            array[i] = array[i + 1];
        }
    }

    // move elements toward left
//  1 2 3 4 5
//  2 3 4 5 1
    void leftShift() {
        int newLast = array[0];
        int shifter = 1;
        while (shifter < array.length) {
            array[shifter - 1] = array[shifter];
            ++shifter;
        }
        array[array.length - 1] = newLast;
    }

    //  move elements towards right
//  1 2 3 4 5
//  5 1 2 3 4
    void rightShift() {
        int lastIndex = array.length - 1;
        int newFirst = array[lastIndex];
        int shifter = lastIndex - 1;
        while (shifter >= 0) {
            array[shifter + 1] = array[shifter];
            --shifter;
        }
        array[0] = newFirst;
    }

    void reverse() {
        int startIndex = 0;
        int lastIndex = array.length - 1;
        while (startIndex < lastIndex) {
            int temp = array[startIndex];
            array[startIndex] = array[lastIndex];
            array[lastIndex] = temp;
            ++startIndex;
            --lastIndex;
        }
    }
}

public class BasicOperations {
    public static void main(String[] args) {
        int[] array = new int[5];
        array[0] = 1;
        array[1] = 2;
        array[2] = 3;
        array[3] = 4;
        array[4] = 5;
        Operations operations = new Operations(array);
        operations.traverse();
        operations.leftShift();
        operations.traverse();
        operations.leftShift();
        operations.traverse();
        operations.rightShift();
        operations.traverse();
        operations.rightShift();
        operations.traverse();
        operations.reverse();
        operations.traverse();

    }
}
