package Array.Searching;

/*
 Sorted Array Required!!
 look at mid, if key is smaller look in left sub-array otherwise on right sub-array
 Best Case: 1
 Average Case: log(N)
 Worst Case: log(N)
 */

class Search {
    int[] array;
    int left;
    int right;
    int mid;
    int key;

    Search(int[] array, int key) {
        this.array = array;
        this.key = key;
        left = 0;
        right = array.length - 1;
    }

    int binarySearch() {
        while (left <= right) {
            mid = (left + right) / 2;
            if (array[mid] == key) {
                return mid;
            } else if (array[mid] < key) {
                left = mid + 1;
            } else if (array[mid] > key) {
                right = mid - 1;
            }
        }
        return -1;
    }
}

public class BinarySearch {
    public static void main(String[] args) {
        Search search = new Search(new int[]{1, 2, 3, 4, 5}, 5);
        System.out.println("Index: "+search.binarySearch());
    }
}
