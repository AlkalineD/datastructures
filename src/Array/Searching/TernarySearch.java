package Array.Searching;

/*
Requires Sorted Array!!
Worst Case: Log3(N)

 */

class Searcher {

    int[] array;

    Searcher(int[] array) {
        this.array = array;
    }

    int ternarySearch(int left, int right, int key) {
        if (left < right) {
            int mid1 = left + (right - left) / 3;
            int mid2 = right - (right - left) / 3;
            /* Found at one of mid points */

            if (array[mid1] == key) {
                return mid1;
            } else if (array[mid2] == key) {
                return mid2;
            }

            // Looking in subArrays

            else if (array[mid1] > key) {
                return ternarySearch(left, mid1 - 1, key);
            } else if (array[mid2] < key) {
                return ternarySearch(mid2 + 1, right, key);
            } else {
                return ternarySearch(mid1 + 1, mid2 - 1, key);
            }
        }
        return -1;
    }

    int ternaryIterative(int left, int right, int key) {
        while (left < right) {
            int mid1 = left + (right - left) / 3;
            int mid2 = right - (right - left) / 3;
            if (array[mid1] == key) {
                return mid1;
            } else if (array[mid2] == key) {
                return mid2;
            } else if (array[mid1] > key) {
                right = mid1 - 1;
            } else if (array[mid2] < key) {
                left = mid2 + 1;
            } else {
                left = mid1 + 1;
                right = mid2 - 1;
            }

        }
        return -1;
    }
}

public class TernarySearch {
    public static void main(String[] args) {
        int[] array = {2, 3, 5, 6, 8, 9, 12, 13, 14};
        int left = 0;
        int right = array.length - 1;
//        System.out.println(array.length);
//        int mid1 = left + right/3;
//        int mid2 = right - right/3;
//        System.out.println(mid1 + " " + mid2);

        Searcher searcher = new Searcher(array);
        System.out.println(searcher.ternaryIterative(left, right, 5));
    }
}
