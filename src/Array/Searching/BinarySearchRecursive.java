package Array.Searching;

class RecursiveSearch {

    RecursiveSearch() {

    }

    int BinarySearch(int[] array, int left, int right, int key) {
        if (left <= right && left < array.length - 1) {
            int mid = (left + right) / 2;
            if (array[mid] == key) {
                return mid;
            } else if (array[mid] > key) {
                return BinarySearch(array, left, mid - 1, key);
            } else {
                return BinarySearch(array, mid + 1, right, key);
            }
        }
        return -1;   // Not Found
    }
}

public class BinarySearchRecursive {
    public static void main(String[] args) {
        RecursiveSearch recursiveSearch = new RecursiveSearch();
        System.out.println(recursiveSearch.BinarySearch(new int[]{1, 2, 3, 4, 5}, 0, 5, 5));
    }
}
