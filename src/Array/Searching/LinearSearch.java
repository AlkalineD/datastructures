package Array.Searching;

import java.util.Scanner;
/*
Just Search iver index if element matches key, Found else Not found in array
 Best Case: 1
 Average Case: N
 Worst Case: N
 */
public class LinearSearch {
    public static void main(String[] args) {
        int[] array = {1, 6, 7, 2, 4, 3, 5, 6};
        boolean foundFlag = false;
        Scanner scanner = new Scanner(System.in);
        int key = Integer.parseInt(scanner.nextLine());
        int index = 0;
        while(index < array.length){
            int element = array[index];
            if(key == element){
                System.out.println("Found at " + index);
                foundFlag = true;
            }
            ++index;
        }

        if(!foundFlag){
            System.out.println("Not Found in Array");
        }
    }
}
