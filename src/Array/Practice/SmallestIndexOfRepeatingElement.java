package Array.Practice;

/*
[1, 5, 3, 4, 5, 2, 3, 1]

Repeating elements => 3, 5, 1
Smallest index among repeating elements = 0 (index of first occurrence of 1)

 */

import java.util.ArrayList;
import java.util.Arrays;

public class SmallestIndexOfRepeatingElement {
    public static void main(String[] args) {
        int[] array = new int[]{1, 5, 3, 4, 5, 2, 3, 1};

        int result = -56;   // minimum index among repeating elements
        ArrayList<Integer> duplicateRecord = new ArrayList<>();

        for (int i = 0; i < array.length; ++i) {
            for (int j = i + 1; j < array.length; ++j) {
                if (array[i] == array[j] && !duplicateRecord.contains(array[i])) {
                    duplicateRecord.add(array[i]);
                }
            }
        }
        System.out.println(duplicateRecord);


    }
}
