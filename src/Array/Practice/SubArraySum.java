package Array.Practice;

import java.util.Scanner;

public class SubArraySum {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        // All input things
        System.out.print("Array Size: ");
        int size = Integer.parseInt(scanner.nextLine());
        System.out.println();
        int[] array = new int[size];
        System.out.println("Array Input");

        for (int i = 0; i < size; ++i) {
            array[i] = Integer.parseInt(scanner.nextLine());
        }

        // surfing sub array
        int subArrSize = size * (size + 1) / 2;

        for (int i = 0; i < size; ++i) {
            int sum = 0;
            for (int j = i; j < size; ++j) {
                sum += array[j];
                System.out.println(sum);
            }
        }


    }
}
