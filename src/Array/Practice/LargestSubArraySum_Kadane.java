package Array.Practice;

// return largest possible subarray sum


// to-do


class LargestSum {

    int kadane() {
        int maxSum = Integer.MIN_VALUE;
        return 0;
    }

    // not right
    void maxSum(int[] array) {
        int maxSum = Integer.MIN_VALUE;
        for (int i = 0; i < array.length; ++i) {
            int sum = 0;
            for (int j = i; j < array.length; ++j) {
                sum += array[j];
            }
            maxSum = Math.max(maxSum, sum);
        }
        System.out.println(maxSum);
    }


}

public class LargestSubArraySum_Kadane {
    public static void main(String[] args) {
        LargestSum largestSum = new LargestSum();
        largestSum.maxSum(new int[]{-2, -3, 4, -1, -2, 1, 5, -3});
    }
}
