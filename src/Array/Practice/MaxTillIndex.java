package Array.Practice;

public class MaxTillIndex {
    static void maxTillI(int[] array){
        int max = array[0];
        int i = 0;
        while(i < array.length) {
            max = Math.max(max, array[i]);
            System.out.println("Max: " +max + " Index: " + i);
            ++i;
        }
    }
    public static void main(String[] args) {
        int[] array = new int[]{0, -9, 1, 3, -4, 5 };
        MaxTillIndex.maxTillI(array);
    }
}
