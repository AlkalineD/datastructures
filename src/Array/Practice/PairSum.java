package Array.Practice;

import java.util.ArrayList;
import java.util.Arrays;

//to-do
class Solution {
    void pairSumOne(int target, int[] array) {
        int pairCount = 0;
        for (int i = 0; i < array.length; ++i) {
            for (int j = i + 1; j < array.length; ++j) {
                if (array[i] + array[j] == target) {
                    ++pairCount;
                    System.out.println(array[i] + " " + array[j]);
                }
            }
        }
        System.out.println("Pair Count: " + pairCount);
    }

    void pairSumTwo(int target, int[] array) {
        Arrays.sort(array);
        int pairCount = 0;
        int low = 0;
        int high = array.length - 1;
        while (low < high) {
            if (array[low] + array[high] == target) {
                ++pairCount;
                System.out.println(array[low] + " " + array[high]);
                // so that we do not go into infinite loop
                ++low;
                --high;
            } else if (array[low] + array[high] > target) {
                --high;
            } else if (array[low] + array[high] < target) {
                ++low;
            }

        }
        System.out.println("Pair Count:" + pairCount);

    }
}

public class PairSum {
    public static void main(String[] args) {
        Solution solution = new Solution();
        solution.pairSumOne(6, new int[]{1, 5, 7, -1, 5});
        solution.pairSumTwo(6, new int[]{1, 5, 7, -1, 5});

    }

}
