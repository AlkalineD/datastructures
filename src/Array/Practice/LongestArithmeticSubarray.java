package Array.Practice;

// In arithmetic array, the difference between all consecutive elements is a constant.
/*
[9, 7, 5 , 3]
9-7 =2
7-5 = 2
5-3 = 2

It is an arithmetic array, constant difference of 2

[1, 2, 4]
1-2 = -1
2-4 = -2

Not an arithmetic array ( difference -2, -1)
 */

public class LongestArithmeticSubarray {
    public static void main(String[] args) {
        int[] array = new int[]{10, 7, 4, 6, 8, 10, 12, 1};
        int difference = array[1] - array[0];
        int result = 2;  // size of longest arithmetic array
        int tempResult = 2;

        for (int i = 2; i < array.length; ++i) {
            int tempDiff = array[i] - array[i - 1];
            if (tempDiff == difference) {
                ++tempResult;
            } else {
                tempResult = 2;
                difference = tempDiff;
            }
            result = Math.max(tempResult, result);
        }
        System.out.println(result);
    }
}
