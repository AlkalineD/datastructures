package Array.Practice;

/*
For array of size n there are n*(n+1) sub arrays
                              -------
                                 2
 */

public class PrintSubArray {
    public static void main(String[] args) {
        int[] array = new int[]{4, 2, 5, 6, 7, 9, 1, 11};
        for (int i = 0; i < array.length; ++i) {
            for (int j = i; j < array.length; ++j) {
                for (int k = i; k <= j; ++k) {
                    System.out.print(array[k] + " ");
                }
                System.out.println();
            }

        }
    }
}
